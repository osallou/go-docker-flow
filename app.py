from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, jsonify

import logging
import logging.config
import os
import sys
import yaml
import time
import datetime
import requests
import jwt
import json

import redis
from pymongo import MongoClient

from prometheus_client.exposition import generate_latest
from prometheus_client import Gauge, Counter, Histogram


default_config = {}

app = Flask(__name__)
app.config['STATIC_FOLDER'] = 'app'
app.config.from_object(default_config)
app.config.from_envvar('GODFLOW_SETTINGS', silent=True)

if 'GOD_INI' in os.environ:
    if not os.path.exists(os.environ['GOD_INI']):
        logging.error(os.environ['GOD_INI']+' not found')
        sys.exit(1)
    else:
        cfg_file = file(os.environ['GOD_INI'])
        with open(f, 'r') as ymlfile:
            app.config['GODCONFIG'] = yaml.load(ymlfile)

else:
    if os.path.exists('go-d.ini'):
        with open('go-d.ini', 'r') as ymlfile:
            app.config['GODCONFIG'] = yaml.load(ymlfile)
    else:
        logging.error('Missing configuration file')
        sys.exit(1)

app.config['QUEUE'] = {}
app.config['FAKE'] = False

mongo = MongoClient(app.config['GODCONFIG']['mongo_url'])
db = mongo[app.config['GODCONFIG']['mongo_db']]
app.config['MONGO'] = db

app.config['REDIS'] = redis.StrictRedis(host=app.config['GODCONFIG']['redis_host'],
                                        port=app.config['GODCONFIG']['redis_port'],
                                        db=app.config['GODCONFIG']['redis_db'])

if 'flow' not in app.config['GODCONFIG']:
    app.config['GODCONFIG']['flow'] = { 'max_parallel_event': 10 }


if app.config['GODCONFIG']['log_config'] is not None:
    for handler in list(app.config['GODCONFIG']['log_config']['handlers'].keys()):
        app.config['GODCONFIG']['log_config']['handlers'][handler] = dict(app.config['GODCONFIG']['log_config']['handlers'][handler])
    logging.config.dictConfig(app.config['GODCONFIG']['log_config'])


app.config['PROMETHEUS'] = Counter('godocker_flow_events',
                                           'Number of GoDocker Flow events')

logger = logging.getLogger('godocker-flow')

logger.warn("GoDocker Flow start up")


def check_event(token, recipe):
    '''
    Check application is allowed to send events to this recipe, get recipe/user info in return
    '''
    web_endpoint = app.config['GODCONFIG']['web_endpoint']

    if app.config['FAKE']:
        user = {
            'id': app.config['GODCONFIG']['flow']['fake_user']
        }
    else:
        jwt_token = jwt.encode({'user': {'app_token': token, 'recipe': recipe},
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                            'aud': 'urn:godocker/api'}, app.config['GODCONFIG']['shared_secret_passphrase'])
        headers = {'Authorization': 'Bearer '+jwt_token}
        user = None
        task_recipe = None

        r = requests.get(web_endpoint + '/api/1.0/3rdparty/token/'+str(token), headers=headers)
        if r.status_code == 200:
            user_json = r.json()
            user = {'id': user_json['id']}
        else:
            return None

    token = jwt.encode({'user': {'id': user['id']},
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                        'aud': 'urn:godocker/api'}, app.config['GODCONFIG']['secret_passphrase'])
    headers = {'Authorization': 'Bearer '+token,
                'Content-type': 'application/json',
                'Accept':'application/json'
                }
    r = requests.get(web_endpoint + '/api/1.0/marketplace/recipe/' + recipe, headers=headers)
    if r.status_code == 200:
        task = r.json()['task']
        r = requests.get(web_endpoint + '/api/1.0/task/' + str(task), headers=headers)
        if r.status_code == 200:
            task_recipe = r.json()
            task_recipe['container']['meta'] = None
            del task_recipe['status']
            del task_recipe['notify']
            if '_id' in task_recipe:
                del task_recipe['_id']
        else:
            return None
    else:
        return None
    return {
        'recipe': task_recipe,
        'user': user
    }

def is_event_valid(event):
    '''
    Check event is valid
    '''

    if 'uri' in event:
        return True
    else:
        return False

def check_header(auth, event):
    '''
    Check authorization and event id match bearer info
    '''
    try:
        decoded_msg = jwt.decode(auth.replace('Bearer ', ''),
                            app.config['GODCONFIG']['shared_secret_passphrase'],
                            audience='urn:godocker/api')
        if not decoded_msg or int(decoded_msg['event']) != int(event):
            return False
    except Exception as e:
        logger.error("Wrong jwt authorization header")
        return False
    return True

def run_task(token):
    '''
    Execute task in GoDocker
    '''
    task = app.config['REDIS'].lpop(app.config['GODCONFIG']['redis_prefix']+':flow:'+token+':events')
    task_json = None
    if task:
        task_json = json.loads(task)
        logger.debug("Submit task: "+task)
        web_endpoint = app.config['GODCONFIG']['web_endpoint']
        token = jwt.encode({'user': {'id': task_json['event']['user']['id']},
                                'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                                'aud': 'urn:godocker/api'}, app.config['GODCONFIG']['secret_passphrase'])
        headers = {'Authorization': 'Bearer '+token}
        task_json['event']['recipe']['user'] = task_json['event']['user']
        task_json['event']['recipe']['requirements']['uris'] = task_json['uri']
        task_json['event']['recipe']['requirements']['godflow'] = token
        private_registry = app.config['GODCONFIG']['use_private_registry']
        if private_registry:
            task_json['event']['recipe']['image']['url'] = task_json['event']['recipe']['image']['url'].replace(private_registry + '/','')

        del task_json['event']['recipe']['user']
        logger.debug("Run task: "+str(task_json['event']))
        # Submit task to godocker
        r = requests.post(web_endpoint + '/api/1.0/task', json.dumps(task_json['event']['recipe']), headers=headers)
        if not r.status_code == 200:
            logger.error('Failed to submit event')
            return None
        task_submit_result = r.json()
        task_json['task'] = task_submit_result['id']
        logger.debug('Event submitted: '+str(task_json['task']))
    return task_json


def check_and_run(token):
    nb_events = app.config['MONGO']['flow_apps'].count({'token': token})
    if nb_events <= app.config['GODCONFIG']['flow']['max_parallel_event']:
        logger.debug("Pull task from queue and execute")
        task = run_task(token)
        if task is not None:
            app.config['MONGO']['flow_apps'].insert(task)

@app.route('/godflow', methods=['GET'])
def home():
    return jsonify({'data': [] })

@app.route('/godflow/api/1.0/ping', methods=['GET'])
def ping():
    return jsonify({})


@app.route('/godflow/api/1.0/<token>/recipe/<recipe>', methods=['POST'])
def event_new(token, recipe):
    '''
    Receive an event:

    curl -H "Content-Type: application/json" -X POST -d '{"uri":["file:///home/osallou/test.py"]}' http://localhost:5000/godflow/api/1.0/123/recipe/test
    '''
    event_info = check_event(token, recipe)
    data = request.json
    if event_info is None or not is_event_valid(data):
        abort(401)

    task_info = json.dumps({
                'token': token,
                'event': event_info,
                'uri': data['uri'],
                'task': None,
    })
    app.config['REDIS'].rpush(app.config['GODCONFIG']['redis_prefix']+':flow:'+token+':events', task_info)
    app.config['PROMETHEUS'].inc()

    check_and_run(token)

    return jsonify({'error':False, 'msg': 'Event received'})

@app.route('/godflow/api/1.0/event/<event>/status/over', methods=['GET'])
def event_over(event):
    '''
    Set a running task as finished, launch a new pending task if needed
    '''
    is_secure = False
    if app.config['FAKE']:
        is_secure = True
    else:
        is_secure = check_header(request.headers['Authorization'], event)
    if not is_secure:
        abort(401)

    event_db = app.config['MONGO']['flow_apps'].find_one({'task': int(event)})
    if event_db is None:
        abort(404)
    app.config['MONGO']['flow_apps'].remove({'task': int(event)})

    check_and_run(event_db['token'])

    return jsonify({})

@app.route('/godflow/api/1.0/<token>/event', methods=['GET'])
def event_info(token):
    '''
    Get general info on pending/running events
    '''
    logger.debug('Get events for '+token)
    nb_running = app.config['MONGO']['flow_apps'].count({'token': token})
    nb_pending = app.config['REDIS'].llen(app.config['GODCONFIG']['redis_prefix']+':flow:'+token+':events')
    return jsonify({'token': token, 'nb_running': nb_running, 'nb_pending': nb_pending})

@app.route('/metrics')
def prometheus():
    return generate_latest()

if __name__ == '__main__':
    debug = False
    if 'GODFLOW_ENV' in os.environ and \
      os.environ['GODFLOW_ENV']=='dev':
        debug = True
    if 'GODFLOW_ENV' in os.environ and \
      os.environ['GODFLOW_ENV']=='fake':
        debug = True
        app.config['FAKE'] = True

    app.run(debug=debug)
