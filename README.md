# About

GoDocker Flow acts as a data flow gateway, queuing and submitting jobs, based on a recipe, for new data.

Events are sent via a HTTP POST to /godflow/api/1.0/<token>/recipe/<recipe>, with body being a JSON list of file URIs.
URIs will be given to the job via local task directory file *godocker-uris.txt*. Job recipe can then use those variables to access/download them and execute its computation.

Token is a unique token for the application sending events and recipe is the recipe id form GoDocker to use.

App needs the godflowwatcher plugin activated in GoDocker watchers

# go-d-flow

This script helps to repair status of events. If godflow is not running while some jobs terminate, application will not receive termination message. Script will check status of events and update events status.

# Status

in development


# python reqs

flask
requests
jwt
PyYAML
redis
pymongo
prometheus_client
