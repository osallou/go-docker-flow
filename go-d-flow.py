import os
import sys
import logging
import yaml
import requests
import jwt
import datetime

from pymongo import MongoClient

def repair(config):
    mongo = MongoClient(config['mongo_url'])
    db = mongo[config['mongo_db']]
    running_jobs = db['flow_apps'].find({})
    fix_counter = 0
    for running_job in running_jobs:
        fix_counter += 1
        godocker_job = db['jobs'].find_one({'id': running_job['task']})
        if godocker_job is None:
            # Job is over, we missed message
            # Send message /godflow/api/1.0/<token>/event/<event>/status/over
            token = jwt.encode({'event': running_job['task'],
                                'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                                'aud': 'urn:godocker/api'}, config['shared_secret_passphrase'])
            headers = {'Authorization': 'Bearer '+token}
            r = requests.get(config['flow']['frontend'] + '/godflow/api/1.0/event/' +str(running_job['task'])+'/status/over', headers=headers)
            if r.status_code != 200:
                logging.error("Failed to update status for event "+str(running_job['task']))
            else:
                logging.info("Fixed event "+str(running_job['task']))
    if fix_counter == 0:
        logging.info("Everything is ok")

if __name__ == "__main__":
    config_file = 'go-d.ini'
    if 'GOD_INI' in os.environ:
        config_file = os.environ['GOD_INI']

    with open(config_file, 'r') as ymlfile:
        config = yaml.load(ymlfile)

    if len(sys.argv) == 2:
        if 'repair' == sys.argv[1]:
            repair(config)
    else:
        print("usage: %s repair" % sys.argv[0])
        sys.exit(2)
